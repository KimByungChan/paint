#ifndef FEATURE_H
#define FEATURE_H

#include <QColor>
class Feature
{
public:
    // [0] ENUM - Type
    enum EFigureType{

        Type_INVALID = -1,

        FeatureShape =100,
        FeatureShape_Rectangle,
        FeatureShape_Circle,
        FeatureShape_Pen
    };

public:
    explicit Feature(int nX, int nY);
    virtual ~Feature();

    QColor color() const;
    void setColor(const QColor &color);

    int x() const;
    void setX(int x);

    int y() const;
    void setY(int y);

    // [5] get Type
    EFigureType type();

private:
    QColor m_color;

protected:
    int m_nX;
    int m_nY;

    // [1] Enum Member Variant.
    Feature::EFigureType m_eType;

};

#endif // FEATURE_H
