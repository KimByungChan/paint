#include "icircle.h"

ICircle::ICircle(int cX,int cY, int Cradius) :
    Feature (cX, cY),
    m_radius(Cradius)
{
    // [4] Circle Type Add
    m_eType = EFigureType::FeatureShape_Circle;
}

ICircle::~ICircle()
{

}

int ICircle::radius() const
{
    return m_radius;
}
