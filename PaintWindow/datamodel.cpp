#include "datamodel.h"
#include "feature.h"
#include "irectangle.h"
#include "icircle.h"
#include "iline.h"

#include <QDebug>

DataModel::DataModel()
{

}

DataModel::~DataModel()
{

}

void DataModel::createRectangle(int nX, int nY, int nWidth, int nHeight, QColor color)
{
    // Create IRectangle;
    IRectangle *pRect = new IRectangle(nX, nY, nWidth, nHeight);
    pRect->setColor(color);

    // Append;
    appendFeature(pRect);
}

void DataModel::createCircle(int cX, int cY, int Cradius, QColor color)
{
    ICircle *pCirc = new ICircle(cX,cY,Cradius);
    pCirc->setColor(color);

    appendFeature(pCirc);
}

void DataModel::createLine(const QList<QPoint> &tPointList, int nThickness, QColor color)
{
    ILine *pLine = new ILine;
    pLine->setPointList(tPointList);
    pLine->setThickNess(nThickness);
    pLine->setColor(color);

    appendFeature(pLine);
}

void DataModel::clear()
{
    foreach(Feature *pFeature, m_tFeatureList)
    {
        delete pFeature;
    }

    m_tFeatureList.clear();
}

void DataModel::appendFeature(Feature *pFeature)
{
     QColor color = pFeature->color();
//    QString strFeatureInfo = QString("X : %1, Y : %2, RGB(%3,%4,%5)").arg(pFeature->x()).arg(pFeature->y()).arg();
//    qDebug() << strFeatureInfo;


    m_tFeatureList.append(pFeature);

}

void DataModel::getFeatures(QList<Feature *> &tFeatureList)
{
    tFeatureList.append(m_tFeatureList);
}

int DataModel::size()
{
    return m_tFeatureList.size();
}
