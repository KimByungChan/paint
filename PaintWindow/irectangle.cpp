#include "irectangle.h"
#include <QDebug>

IRectangle::IRectangle(int nX, int nY, int nWidth, int nHeight) :
    Feature (nX, nY),
    m_nWidth(nWidth),
    m_nHeight(nHeight)
{
    // [3] Rectangle Type Add
    m_eType = EFigureType::FeatureShape_Rectangle;
    qDebug() << "Create" << m_nX;
}

IRectangle::~IRectangle()
{
    qDebug() << "Destory" << m_nX;
}

int IRectangle::width() const
{
    return m_nWidth;
}

int IRectangle::height() const
{
    return m_nHeight;
}
