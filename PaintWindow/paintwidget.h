#ifndef PAINTWIDGET_H
#define PAINTWIDGET_H

#include <QWidget>

class QActionGroup;
class DataModel;
class PaintWidget : public QWidget
{
    Q_OBJECT
public:
    explicit PaintWidget(QWidget *parent = nullptr);
    virtual ~PaintWidget();

    void setModel(DataModel *pModel);


signals:
    void clicked(QPoint point);
    void clicked(QList<QPoint> tPointList);

public slots:
    void setDragEnable(bool bDragEnable);

    // QWidget interface
protected:
    void paintEvent(QPaintEvent *event);

    // QWidget interface
protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

private:

    QList<QPoint> m_tTempPointList;
    DataModel *m_pModel;
    QActionGroup *m_pFigureGroup;

    bool m_bIsClicked;
    bool m_bDragEnable;

};

#endif // PAINTWIDGET_H
