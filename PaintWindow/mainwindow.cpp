#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "paintwidget.h"
#include "irectangle.h"
#include "icircle.h"
#include <QDebug>

#define _ACTION_ATTRIBUTE "ACTION_ATTRIBUTE"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_pPaintWidget(new PaintWidget)
{
    ui->setupUi(this);
    setCentralWidget(m_pPaintWidget);

    initModel();
    initActions();

    m_pPaintWidget->setModel(&m_model);
    updateTitle();

    connect(m_pPaintWidget,SIGNAL(clicked(QPoint)), this, SLOT(clickedWidget(QPoint)));
    connect(m_pPaintWidget,SIGNAL(clicked(QList<QPoint>)), this, SLOT(clickedWidget(QList<QPoint>)));
    connect(ui->actionPen, SIGNAL(changed()), this, SLOT(setDragState()));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initModel()
{
}

void MainWindow::initActions()
{
    m_pToolGroup = new QActionGroup(this); // Tool Group

    m_pToolGroup->addAction(ui->actionPen);
    m_pToolGroup->addAction(ui->actionRectangle);
    m_pToolGroup->addAction(ui->actionCircle);

    ui->actionPen->setProperty(_ACTION_ATTRIBUTE, static_cast<int>(E_ACTION_TOOL_PEN));
    ui->actionRectangle->setProperty(_ACTION_ATTRIBUTE, static_cast<int>(E_ACTION_TOOL_RECTANGLE));
    ui->actionCircle->setProperty(_ACTION_ATTRIBUTE, static_cast<int>(E_ACTION_TOOL_CIRCLE));
    // ui->actionCircle->setProperty(_ACTION_ATTRIBUTE, static_cast<int>(E_ACTION_TOOL_Clear));


    m_pColorGroup = new QActionGroup(this); // Color Group

    m_pColorGroup->addAction(ui->actionRed);
    m_pColorGroup->addAction(ui->actionBlue);
    m_pColorGroup->addAction(ui->actionGreen);
    m_pColorGroup->addAction(ui->actionYellow);

    ui->actionRed->setProperty(_ACTION_ATTRIBUTE, static_cast<int>(E_ACTION_COLOR_Red));
    ui->actionGreen->setProperty(_ACTION_ATTRIBUTE, static_cast<int>(E_ACTION_COLOR_Green));
    ui->actionBlue->setProperty(_ACTION_ATTRIBUTE, static_cast<int>(E_ACTION_COLOR_Blue));
    ui->actionYellow->setProperty(_ACTION_ATTRIBUTE, static_cast<int>(E_ACTION_COLOR_Yellow));

    m_pThickNessGroup = new QActionGroup(this); // ThickNess Group

    m_pThickNessGroup->addAction(ui->actionSmall);
    m_pThickNessGroup->addAction(ui->actionNormal);
    m_pThickNessGroup->addAction(ui->actionBig);

    ui->actionSmall->setProperty(_ACTION_ATTRIBUTE, static_cast<int>(E_ACTION_ThickNess_Small));
    ui->actionNormal->setProperty(_ACTION_ATTRIBUTE, static_cast<int>(E_ACTION_ThickNess_Normal));
    ui->actionBig->setProperty(_ACTION_ATTRIBUTE, static_cast<int>(E_ACTION_ThickNess_Big));

}

void MainWindow::on_actionClear_triggered()
{
    m_model.clear();

    updateTitle();
    m_pPaintWidget->repaint();

}

QColor MainWindow::getColor()
{
    QAction *pColorAction = m_pColorGroup->checkedAction();
    if(!pColorAction)
    {
        return QColor();
    }

    int nValue_1 = pColorAction->property(_ACTION_ATTRIBUTE).toInt();
    EActionAtrribute eColorAttrbute = static_cast<EActionAtrribute>(nValue_1);

    QColor color;
    switch (eColorAttrbute) {
    case E_ACTION_COLOR_Red:    color = QColor(Qt::red);    break;
    case E_ACTION_COLOR_Green:  color = QColor(Qt::green);  break;
    case E_ACTION_COLOR_Blue:   color = QColor(Qt::blue);   break;
    case E_ACTION_COLOR_Yellow: color = QColor(Qt::yellow); break;

    default:
        break;
    }

    return color;
}

int MainWindow::getThickNess()
{
    QAction *pThickNessAction = m_pThickNessGroup->checkedAction();
    if(!pThickNessAction)
    {
        return -1;
    }

    int nValue_2 = pThickNessAction->property(_ACTION_ATTRIBUTE).toInt();
    EActionAtrribute eThickNessAttrbute = static_cast<EActionAtrribute>(nValue_2);

    int size=0;
    switch (eThickNessAttrbute) {
    case E_ACTION_ThickNess_Small: size = 10;  break;
    case E_ACTION_ThickNess_Normal: size = 20;  break;
    case E_ACTION_ThickNess_Big: size = 30;  break;

    default:
        break;
    }

    return size;
}

void MainWindow::clickedWidget(QPoint point)
{
    //    m_pToolGroup->checkedAction();
    QAction *pToolAction = m_pToolGroup->checkedAction();
    if(!pToolAction)
    {
        return;
    }

    QColor color = getColor();
    if(!color.isValid())
    {
        return;
    }

    int size = getThickNess();
    if(size < 0)
    {
        return;
    }


    int x = point.x();
    int y = point.y();

    int nValue = pToolAction->property(_ACTION_ATTRIBUTE).toInt();
    EActionAtrribute eToolAttrbute = static_cast<EActionAtrribute>(nValue);

    switch (eToolAttrbute) {
    case E_ACTION_TOOL_PEN:
    {
        //m.model.createPen();
    }
        break;

    case E_ACTION_TOOL_RECTANGLE:
    {
        m_model.createRectangle(x,y, size, size, color);
    }
        break;

    case E_ACTION_TOOL_CIRCLE:
    {
        m_model.createCircle(x,y, size / 2, color);
    }
        break;

    default:
        break;
    }

    updateTitle();
}

void MainWindow::clickedWidget(QList<QPoint> tPointList)
{
    QColor color = getColor();
    if(!color.isValid())
    {
        return;
    }

    int size = getThickNess();
    if(size < 0)
    {
        return;
    }

    m_model.createLine(tPointList, size / 10, color);
}

void MainWindow::setDragState()
{
    bool bIsChecked = ui->actionPen->isChecked();
    m_pPaintWidget->setDragEnable(bIsChecked);
}

void MainWindow::updateTitle()
{

    int nSize = m_model.size();
    QString strTitle = QString("Simple Paint - Feature Count (%1)").arg(nSize);
    setWindowTitle(strTitle);

}
