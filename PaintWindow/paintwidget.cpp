#include "datamodel.h"
#include "feature.h"
#include "icircle.h"
#include "irectangle.h"
#include "paintwidget.h"
#include "mainwindow.h"
#include "iline.h"
#include <QPainter>
#include <QDebug>
#include <qevent.h>

PaintWidget::PaintWidget(QWidget *parent) : QWidget(parent),
    m_pModel(nullptr),
    m_bIsClicked(false),
    m_bDragEnable(false)
{
    setMouseTracking(true);
}

PaintWidget::~PaintWidget()
{

}

void PaintWidget::paintEvent(QPaintEvent *event)
{
    // 1. Background
    QPainter painter;
    painter.begin(this);

    QPen pen;
    pen.setWidth(0);
    pen.setColor(Qt::gray);

    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(Qt::gray);

    painter.setPen(pen);
    painter.setBrush(brush);

    // 1-1. Draw Background
    painter.drawRect(0,0, width(), height());

    if(!m_pModel)
    {
        painter.end();
        return;
    }


    pen.setWidth(1);
    painter.setBrush(Qt::NoBrush);

    QList<Feature *> tFeatureList;
    m_pModel->getFeatures(tFeatureList);
    foreach( Feature *pFeature, tFeatureList)
    {
        if(!pFeature)
            continue;

        QColor color = pFeature->color();
        pen.setColor(color);
        painter.setPen(pen);

        int x = pFeature->x();
        int y = pFeature->y();

        // [6] get Type
        // [7] Switch
        switch ((pFeature->type())) {
        case Feature::FeatureShape_Rectangle:
        {
            // [7-1] Rectangle Draw
            // width, height
            IRectangle* rectangle = dynamic_cast<IRectangle *>(pFeature);
            if(!rectangle)
            {
                continue;
            }

            int nWidth = rectangle->width();
            int nHeight = rectangle->height();

            painter.drawRect(x,y, nWidth, nHeight);

        }
            break;
        case Feature::FeatureShape_Circle:
        {
            ICircle* circle = dynamic_cast<ICircle *>(pFeature);
            if(!circle)
            {
                continue;
            }

            int Cradius = circle-> radius();

            painter.drawEllipse(x,y, Cradius * 2 , Cradius * 2 );
        }
            break;


        case Feature::FeatureShape_Pen:
        {
            ILine* line = dynamic_cast<ILine *>(pFeature);
            if(!line)
            {
                continue;
            }

            QList<QPoint> tPointList = line->pointList();
            int nSize = line->thickNess();

            pen.setWidth(nSize);
            pen.setColor(color);
            painter.setPen(pen);

            QPoint prevPoint;
            foreach(QPoint point, tPointList)
            {
                if(prevPoint.isNull())
                {
                    prevPoint = point;
                    continue;
                }

                painter.drawLine(prevPoint, point);
                prevPoint = point;
            }


        }
            break;

        default:
            break;
        }
    }

    pen.setWidth(0);
    pen.setColor(Qt::black);
    painter.setPen(pen);

    QPoint prevPoint;
    foreach(QPoint point, m_tTempPointList)
    {
        if(prevPoint.isNull())
        {
            prevPoint = point;
            continue;
        }

        painter.drawLine(prevPoint, point);
        prevPoint = point;
    }



    qDebug() << "Width : " << width() << ", Height : " << height();

    painter.end();
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
    qDebug() << "Mouse eve!!";
    m_bIsClicked = true;

    qDebug() << "Mouse Clicked!!";

    QPoint point = event->pos();
    emit clicked(point);

    if(m_bDragEnable)
    {
        QPoint point = event->pos();
        m_tTempPointList.append(point);
    }

    repaint();
}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
    if(m_bIsClicked && m_bDragEnable)
    {
        QPoint point = event->pos();
        m_tTempPointList.append(point);

        repaint();
    }
}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
    m_bIsClicked = false;

    if(m_bDragEnable)
    {
        QPoint point = event->pos();
        m_tTempPointList.append(point);

        emit clicked(m_tTempPointList);

        m_tTempPointList.clear();
    }

    repaint();
}

void PaintWidget::setDragEnable(bool bDragEnable)
{
    m_bDragEnable = bDragEnable;
}

void PaintWidget::setModel(DataModel *pModel)
{
    m_pModel = pModel;
}
