#ifndef IRECTANGLE_H
#define IRECTANGLE_H

#include "feature.h"

class IRectangle : public Feature
{
public:
    explicit IRectangle(int x, int y, int width, int height);
    virtual ~IRectangle();

    int width() const;

    int height() const;

private:
    int m_nWidth;
    int m_nHeight;
};

#endif // IRECTANGLE_H




