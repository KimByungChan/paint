#include "feature.h"

Feature::Feature(int nX, int nY) :
    m_nX(nX),
    m_nY(nY),
    m_eType(Type_INVALID)
  // [2] Enum Member Variant init INVALID
{

}

Feature::~Feature()
{

}

QColor Feature::color() const
{
    return m_color;
}

void Feature::setColor(const QColor &color)
{
    m_color = color;
}

int Feature::x() const
{
    return m_nX;
}

void Feature::setX(int nX)
{
    m_nX = nX;
}

int Feature::y() const
{
    return m_nY;
}

void Feature::setY(int nY)
{
    m_nY = nY;
}

Feature::EFigureType Feature::type()
{
    return m_eType;
}
