#ifndef ILINE_H
#define ILINE_H
#include "feature.h"
#include <QList>

class ILine : public Feature
{
public:
    explicit  ILine();
    virtual ~ILine();


    QList<QPoint> pointList() const;
    void setPointList(const QList<QPoint> &pointList);

    int thickNess() const;
    void setThickNess(int thickNess);

private:

    QList<QPoint> m_tPointList;
    int m_nThickNess;
};

#endif // ILINE_H
