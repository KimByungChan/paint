#ifndef ICIRCLE_H
#define ICIRCLE_H

#include "feature.h"

class ICircle : public Feature
{
public:
    explicit ICircle(int cX, int cY,int Cradius);
    virtual ~ICircle();

    int radius() const;

private:
    int m_radius;
};

#endif // ICIRCLE_H
