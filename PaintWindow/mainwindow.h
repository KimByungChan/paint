#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "datamodel.h"

#include <QMainWindow>

class QActionGroup;
class PaintWidget;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow // Parent Class : Qmainwindow,  Children Class : Mainwindow
{
    Q_OBJECT

    enum EActionAtrribute {
        E_ACTION_INVALID = -1,

        E_ACTION_TOOL = 100,
        E_ACTION_TOOL_PEN ,
        E_ACTION_TOOL_RECTANGLE,
        E_ACTION_TOOL_CIRCLE,
         E_ACTION_TOOL_Clear,

        E_ACTION_COLOR = 1000,
        E_ACTION_COLOR_Red,
        E_ACTION_COLOR_Green,
        E_ACTION_COLOR_Blue,
        E_ACTION_COLOR_Yellow,

        E_ACTION_ThickNess = 10000,
        E_ACTION_ThickNess_Small,
        E_ACTION_ThickNess_Normal,
        E_ACTION_ThickNess_Big,
    };


public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void initModel();
    void initActions();

    int getThickNess();
    QColor getColor();

private slots:
    void on_actionClear_triggered();

    void clickedWidget(QPoint point);

    void clickedWidget(QList<QPoint> tPointList);

    void setDragState();

    void updateTitle();
private:
    Ui::MainWindow *ui;
    PaintWidget *m_pPaintWidget;

    QActionGroup *m_pToolGroup;
    QActionGroup *m_pColorGroup;
    QActionGroup *m_pThickNessGroup;
    DataModel m_model;
};

#endif // MAINWINDOW_H
