#ifndef DATAMODEL_H
#define DATAMODEL_H

#include <QList>


class Feature; // Global Class?
class DataModel
{
public:
    explicit DataModel();
    ~DataModel();

    // [3] parameter change
    void createRectangle(int nX, int nY, int nWidth, int nHeight, QColor color);
    void createCircle(int cX, int cY, int Cradius, QColor color);
    void createLine(const QList<QPoint> &tPointList, int nThickness, QColor color);


    void clear();
    void appendFeature(Feature * pFeature);
    void getFeatures(QList<Feature *> &tFeatureList);

    int size();

private:
    QList <Feature *> m_tFeatureList;
};

#endif // DATAMODEL_H
