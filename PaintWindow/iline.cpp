#include "iline.h"

ILine::ILine() :
    Feature (-1,-1),
    m_nThickNess(0)
{
    m_eType = EFigureType::FeatureShape_Pen;
}

ILine::~ILine()
{

}

QList<QPoint> ILine::pointList() const
{
    return m_tPointList;
}

void ILine::setPointList(const QList<QPoint> &tPointList)
{
    m_tPointList = tPointList;
}

int ILine::thickNess() const
{
    return m_nThickNess;
}

void ILine::setThickNess(int nThickNess)
{
    m_nThickNess = nThickNess;
}
